import sys
import pygame
from pygame.locals import *
import math
import os
#os.environ["SDL_VIDEODRIVER"] = "dummy"




def dibujar_palo(ancho, alto, color, palo, pantalla):

    medio_ancho = int(ancho/2)
    octavo_ancho = int(ancho/8)


    medio_alto = int(alto/2)
    octavo_alto = int(alto/8)

    trazo = 4

    if palo == 0:
        ## Diamante
        pygame.draw.polygon(pantalla, color,
                            ((medio_ancho, octavo_alto), (2*octavo_ancho, medio_alto),
                             (medio_ancho, 7*octavo_alto), (6*octavo_ancho, medio_alto)), trazo)

    elif palo == 1:
        ## Corazón
        pygame.draw.line(pantalla, color,
                         (octavo_ancho, 3*octavo_alto), (medio_ancho, 7*octavo_alto), trazo)
        pygame.draw.line(pantalla, color,
                         (medio_ancho, 7*octavo_alto), (7*octavo_ancho, 3*octavo_alto), trazo)
        pygame.draw.line(pantalla, color,
                         (3*octavo_ancho, octavo_alto), (medio_ancho, medio_alto), trazo)
        pygame.draw.line(pantalla, color,
                         (medio_ancho, medio_alto), (5*octavo_ancho, octavo_alto), trazo)
        pygame.draw.arc(pantalla, color,
                        pygame.rect.Rect(octavo_ancho, octavo_alto, 4*octavo_ancho, 4*octavo_alto),
                        math.radians(90), math.radians(180), trazo)
        pygame.draw.arc(pantalla, color,
                        pygame.rect.Rect(3*octavo_ancho, octavo_alto, 4*octavo_ancho, 4*octavo_alto),
                        math.radians(0), math.radians(90), trazo)
    elif palo == 2:
        ## Tréboles
        pygame.draw.circle(pantalla, color, (2*octavo_ancho, medio_alto), octavo_ancho, trazo)
        pygame.draw.circle(pantalla, color, (6*octavo_ancho, medio_alto), octavo_ancho, trazo)
        pygame.draw.circle(pantalla, color, (medio_ancho, 2*octavo_alto), octavo_ancho, trazo)
        pygame.draw.polygon(pantalla, color, ((medio_ancho, medio_alto), (3*octavo_ancho, 7*octavo_alto), (5*octavo_ancho, 7*octavo_alto)), trazo)
                               
    elif palo == 3:
        ## Picas
        pygame.draw.arc(pantalla, color,
                        pygame.rect.Rect(octavo_ancho, 2*octavo_alto, 3*octavo_ancho, 3*octavo_alto),
                        math.radians(195), math.radians(0), trazo)
        pygame.draw.arc(pantalla, color,
                        pygame.rect.Rect(medio_ancho, 2*octavo_alto, 3*octavo_ancho, 3*octavo_alto),
                        math.radians(180), math.radians(345), trazo)
        pygame.draw.polygon(pantalla, color, ((medio_ancho, medio_alto), (3*octavo_ancho, 7*octavo_alto), (5*octavo_ancho, 7*octavo_alto)), trazo)
        pygame.draw.line(pantalla, color, (octavo_ancho, medio_alto), (medio_ancho, octavo_alto), trazo)
        pygame.draw.line(pantalla, color, (medio_ancho, octavo_alto), (7*octavo_ancho, medio_alto), trazo)
                        


def main():

    ANCHO = 400
    ALTO = 400

    pantalla = pygame.display.set_mode(400, 400)
    pygame.display.set_caption('Palos de la baraja francesa')
    pantalla.fill((255,255,255))

    while True:
        for eventos in pygame.event.get():
            if eventos.type == QUIT:
                sys.exit(0)

        for i in range(4):
                dibujar_palo(400, 400, (255,0,0), i, pantalla)
                pygame.display.update()
                pygame.time.delay(1500)
                pantalla.fill((255,255,255))


if __name__=='__main__':
    pygame.init()
    main()
    
    
