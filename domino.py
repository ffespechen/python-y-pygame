# Fuente (Sweet Hipster) descargada de https://www.1001freefonts.com/

import sys
import pygame
from pygame.locals import *
import random


ANCHO = 400
ALTURA = 600

FICHA_ANCHO = 40
FICHA_ALTO = 75

SEPARADOR = 10



class Ficha(pygame.sprite.Sprite):

    def __init__(self, posx, posy, anchof, altof, num_arriba, num_abajo):
        pygame.sprite.Sprite.__init__(self)
        self.left = posx
        self.top = posy
        self.anchof = anchof
        self.altof = altof
        self.num_arriba = str(num_arriba)
        self.num_abajo = str(num_abajo)
 

    def update(self, pantalla):
        ## Pinto la ficha de blanco
        for i in range(self.left, self.left+self.anchof):
            pygame.draw.line(pantalla, (255,255,153), (i,self.top), (i, self.top+self.altof),2)

        pygame.draw.line(pantalla, (0,0,0),
                         (self.left, self.top+int((self.altof/2))),
                         (self.left+self.anchof, self.top+int(self.altof/2)),
                         4)

        ## Número de arriba
        fuente = pygame.font.Font('SweetHipster.ttf', 30)
        imagen_texto1 = pygame.font.Font.render(fuente, self.num_arriba, 1, (255,0,0))
        imagen_rect = imagen_texto1.get_rect()
        imagen_rect.centerx = self.left+int(self.anchof/2)
        imagen_rect.centery = self.top+int(self.altof/4)
        pantalla.blit(imagen_texto1, imagen_rect)

        
        fuente = pygame.font.Font('SweetHipster.ttf', 30)
        imagen_texto2 = pygame.font.Font.render(fuente, self.num_abajo, 1, (0,153,0))
        imagen_rect = imagen_texto2.get_rect()
        imagen_rect.centerx = self.left+int(self.anchof/2)
        imagen_rect.centery = self.top+int(3*self.altof/4)
        pantalla.blit(imagen_texto2, imagen_rect)

        


def main():
    ventana = pygame.display.set_mode((ANCHO, ALTURA))
    pygame.display.set_caption('Fichas de Dominó con PyGame')

    
    ventana.fill((0,0,0))


    distancia_vertical = 10
    distancia_horizontal = 10
    fichas = [list() for k in range(7)]
    paso = 0
    

    for i in range(paso, 7):
        paso = i
        for j in range(paso, 7):
            fichas[i].append(Ficha(distancia_horizontal, distancia_vertical, FICHA_ANCHO, FICHA_ALTO, i, j))
            distancia_horizontal += (FICHA_ANCHO+SEPARADOR)
        distancia_vertical += (FICHA_ALTO+SEPARADOR)
        distancia_horizontal = 10
 
    
    

    while True:
        #-- detecto eventos
        for eventos in pygame.event.get():
            if eventos.type == QUIT:
                sys.exit(0)

        for lista in fichas:
            for elemento in lista:
                elemento.update(ventana)
        
        pygame.display.update()

    return 0


if __name__=='__main__':
    pygame.init()
    main()
    

                
